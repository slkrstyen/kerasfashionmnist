"""
    Keras Fashion MNIST
    Based on 
    https://machinelearningmastery.com/handwritten-digit-recognition-using-convolutional-neural-networks-python-keras/
"""

from keras.datasets import fashion_mnist
import matplotlib.pyplot as plt

# Keras has a built in way to load the Fashion MNIST dataset
# Downloads the first time and then stores in file
(X_train, y_train), (X_test, y_test) = fashion_mnist.load_data()

# Plot the first couple of images
# Images are 28x28 pixels

plt.subplot(221)
plt.imshow(X_train[0], cmap=plt.get_cmap('gray'))
plt.subplot(222)
plt.imshow(X_train[1], cmap=plt.get_cmap('gray'))
plt.subplot(223)
plt.imshow(X_test[0], cmap=plt.get_cmap('gray'))
plt.subplot(224)
plt.imshow(X_test[1], cmap=plt.get_cmap('gray'))
plt.show()

# First we are going to create a multi-layer perceptron.
import numpy
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.utils import np_utils

# Good idea to initialize random number generator with a constant to make sure 
# results are reproducible. 
seed = 7;
numpy.random.seed(seed)

# Flatten each image to 784 vector
num_pixels = X_train.shape[1] * X_train.shape[2]
X_train = X_train.reshape(X_train.shape[0], num_pixels).astype('float32')
X_test = X_test.reshape(X_test.shape[0], num_pixels).astype('float32')

# The pixel values are 0-255 grayscale so normalize to be between 0 and 1
X_train = X_train/255
X_test = X_test/255

# Outputs are 0-9. Use one hot encoding.
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
num_classes = y_test.shape[1]

# Baseline model
def baseline_model():
    # Create the model
    model = Sequential()
    model.add(Dense(num_pixels, input_dim=num_pixels, kernel_initializer='normal', activation='relu'))
    model.add(Dense(num_classes, kernel_initializer='normal', activation='softmax'))
    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

# Fit and evaluate the model
model = baseline_model()
# Fit the model
model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=2)
# Finally evaluate the model
scores = model.evaluate(X_test, y_test, verbose=0)
print("Baseline Model Error: %.2f%%" % (100-scores[1]*100))
# Error rate of about 1.85%

# Now we are going to implement a simple CNN for MNIST
# which will include Convolutional, Pooling, and Dropout layers.
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras import backend as K
K.set_image_dim_ordering('th')

# Reload and reshape the data 
(X_train, y_train), (X_test, y_test) = fashion_mnist.load_data()
# Reshape to be [samples][pixels][width][height]
X_train = X_train.reshape(X_train.shape[0],1,28,28).astype('float32')
X_test = X_test.reshape(X_test.shape[0],1,28,28).astype('float32')

# Once again normalize inputs
X_train = X_train/255
X_test = X_test/255
# And also one hot encode outputs
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)

def baseline_cnn_model():
    # Create model
    model = Sequential()
    model.add(Conv2D(32, (5,5), input_shape=(1, 28, 28), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))
    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

# Build the model 
model = baseline_cnn_model()
# Fit the model
model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=2)
# Evaluate the model
scores = model.evaluate(X_test, y_test, verbose=0)
print("Baseline CNN Error: %.2f%%" % (100-scores[1]*100))
# 0.98% error



